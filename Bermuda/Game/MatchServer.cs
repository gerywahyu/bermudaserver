using System.Collections.Generic;
using Bermuda.Utils;

namespace Bermuda.Game
{
    public class MatchServer
    {
        private static MatchServer _instance;
        public readonly Dictionary<string, Room> Rooms = new Dictionary<string, Room>();
        
        public static MatchServer GetInstance()
        {
            return _instance ?? (_instance = new MatchServer());
        }

        private MatchServer()
        {
            
        }

        public void CreateRoom(int matchId, string roomId)
        {
            Rooms.Add(roomId, new Room(roomId, matchId));
        }

        public void StartMatch(string roomId)
        {
            Rooms[roomId].Start();
        }

        public void AddPlayer(string roomId, string username, int team)
        {
            Rooms[roomId].AddPlayer(username, team);
        }

        public void UpdateRoomInstance(string roomId, IEnumerable<ObjectInstance> instances)
        {
            Rooms[roomId].UpdateInstance(instances);
        }

        public List<Room> GetWaitingMatches()
        {
            var results = new List<Room>();
            foreach (var (_, room) in Rooms)
            {
                if (room.Status == "waiting")
                {
                    results.Add(room);
                }
            }

            return results;
        }
    }
}