using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bermuda.Game
{
    public class GameState
    {
        [JsonProperty("players")]
        public List<ObjectInstance> Objects { get; set; }
    }
}