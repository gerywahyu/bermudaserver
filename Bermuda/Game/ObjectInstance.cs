using System.ComponentModel;
using Bermuda.Models;
using Newtonsoft.Json;

namespace Bermuda.Game
{
    public class ObjectInstance
    {
        [DefaultValue(-1)]
        [JsonProperty(PropertyName = "id", DefaultValueHandling = DefaultValueHandling.Populate)]
        public int Id { get; set; }
        
        [JsonProperty(PropertyName = "status")]
        public bool Status { get; set; }
        
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }
        
        [JsonProperty(PropertyName = "position")]
        public Position Position { get; set; }
        
        [JsonProperty(PropertyName = "angle")]
        public double Angle { get; set; }

        public ObjectInstance(string username)
        {
            Username = username;
        }
    }
}