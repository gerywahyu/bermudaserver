using System.Collections.Generic;
using System.Linq;
using Bermuda.Game;

namespace Bermuda.Game
{
    public class Room
    {
        private string _id;
        private int _matchId;
        private readonly Dictionary<int, ObjectInstance> _objects = new Dictionary<int, ObjectInstance>();
        private int _lastId = 0;
        public string Status;

        public Room(string id, int matchId)
        {
            _id = id;
            _matchId = matchId;
            Status = "waiting";
        }

        public void AddPlayer(string username, int team)
        {
            _objects.Add(_lastId, new PlayerInstance(username, team));
            _lastId++;
        }

        public void UpdateInstance(IEnumerable<ObjectInstance> instances)
        {
            foreach (var instance in instances)
            {
                if (instance.Id != -1)
                {
                    _objects[instance.Id] = instance;
                }
                else
                {
                    _objects.Add(_lastId, instance);
                    _lastId++;
                }
            }
                
        }

        public void Start()
        {
            Status = "start";
        }

        public void SendInstances()
        {
            Status = "active";
        }

        public GameState GetGameState()
        {
            var state = new GameState {Objects = _objects.Values.ToList()};
            return state;
        }
    }
}