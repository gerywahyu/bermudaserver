using Newtonsoft.Json;

namespace Bermuda.Game
{
    public class PlayerInstance : ObjectInstance
    {
        [JsonProperty(PropertyName = "team")] public int Team;

        public PlayerInstance(string username, int team) : base(username)
        {
            Team = team;
        }
    }
}