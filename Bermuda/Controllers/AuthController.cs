using Bermuda.Models.Requests;
using Bermuda.Models.Responses;
using Bermuda.Repositories;
using Bermuda.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Bermuda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly UserRepository _userRepository = new UserRepository(DatabaseUtils.GetDbConnection());

        [HttpPost("register")]
        public ActionResult Post([FromBody] CreateUserRequest request)
        {
            var success = _userRepository.CreateUser(request.Username, request.Password);
            var response = success ? new CreateUserResponse(true) : new CreateUserResponse(false);
            return new JsonResult(response);
        }

        [HttpPost("login")]
        public ActionResult Post([FromBody] LoginUserRequest request)
        {
            var user = _userRepository.GetUser(request.Username);
            var response = user.Password == request.Password
                ? new LoginUserResponse(true)
                : new LoginUserResponse(false);
            return new JsonResult(response);
        }
    }
}