using Bermuda.Repositories;
using Bermuda.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Bermuda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MapController
    {
        private readonly MapRepository _mapRepository = new MapRepository(DatabaseUtils.GetDbConnection());
        
        [HttpGet("{matchId}")]
        public ActionResult Get(int matchId)
        {
            var map = _mapRepository.GetMap(matchId);
            return new JsonResult(map);
        }
    }
}