using System.Collections.Generic;
using Bermuda.Game;
using Bermuda.Models.Requests;
using Bermuda.Models.Responses;
using Bermuda.Repositories;
using Bermuda.Utils;
using Microsoft.AspNetCore.Mvc;

namespace Bermuda.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MatchController : ControllerBase
    {
        private readonly MatchRepository _matchRepository = new MatchRepository(DatabaseUtils.GetDbConnection());
        private readonly MapRepository _mapRepository = new MapRepository(DatabaseUtils.GetDbConnection());
        
        [HttpPost("create")]
        public ActionResult Post([FromBody] CreateMatchRequest request)
        {
            var match = _matchRepository.CreateMatch(request.Username);
            if (match != null)
            {
                var map = _mapRepository.CreateMap(match.Id);
            }
            return match == null
                ? new JsonResult(new CreateMatchResponse(false, null))
                : new JsonResult(new CreateMatchResponse(true, match));
        }

        [HttpPost("update")]
        public ActionResult Post([FromBody] UpdateInstanceRequest request)
        {
            var matchServer = MatchServer.GetInstance();
            matchServer.UpdateRoomInstance(request.RoomId, request.Instances);
            
            return new JsonResult(new UpdateInstanceResponse(true));
        }

        [HttpPost("start")]
        public ActionResult Post([FromBody] StartMatchRequest request)
        {
            var matchServer = MatchServer.GetInstance();
            matchServer.StartMatch(request.RoomId);
            
            return new JsonResult(new StartMatchResponse(true));
        }

        [HttpGet]
        public ActionResult Get()
        {
            var matchServer = MatchServer.GetInstance();
            var matches = matchServer.GetWaitingMatches();
            return new JsonResult(matches);
        }
    }
}