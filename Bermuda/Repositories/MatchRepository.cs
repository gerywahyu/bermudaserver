using System.Collections.Generic;
using Bermuda.Game;
using Bermuda.Models;
using Bermuda.Utils;
using MySql.Data.MySqlClient;

namespace Bermuda.Repositories
{
    public class MatchRepository
    {
        private readonly MySqlConnection _conn;

        public MatchRepository(MySqlConnection conn)
        {
            _conn = conn;
        }

        private Match GetLatestMatch()
        {
            const string query = "SELECT * FROM `match` WHERE id=LAST_INSERT_ID()";
            
            var command = new MySqlCommand(query, _conn);
            var reader = command.ExecuteReader();
            var matches = ModelUtils.GetObjects<Match>(reader);
            reader.Close();

            if (matches.Count == 0) return null;

            var match = matches[0];
            match.Players = GetPlayers(match.Id);

            return match;
        }

        public Match CreateMatch(string username)
        {
            var roomId = ModelUtils.RandomString(5);
            const string query = "INSERT INTO `match` (master_username, created_at, room_id) VALUES (@master_username, NOW(), @roomId)";
            
            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@master_username", username);
            command.Parameters.AddWithValue("@roomId", roomId);
            var affectedRows = command.ExecuteNonQuery();

            if (affectedRows == 0) return null;

            var match = GetLatestMatch();
            var matchServer = MatchServer.GetInstance();
            matchServer.CreateRoom(match.Id, roomId);
            
            AddPlayer(username, match.Id, 0, roomId);
            
            return match;
        }

        public Match GetMatch(int id)
        {
            const string query = "SELECT * FROM `match` WHERE id=@id";

            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@id", id);
            var reader = command.ExecuteReader();
            var matches = ModelUtils.GetObjects<Match>(reader);
            reader.Close();

            if (matches.Count == 0) return null;

            var match = matches[0];
            match.Players = GetPlayers(id);

            return match;
        }

        public List<Player> GetPlayers(int matchId)
        {
            const string query = "SELECT * FROM player WHERE match_id=@match_id";
            
            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@match_id", matchId);
            var reader = command.ExecuteReader();
            var players = ModelUtils.GetObjects<Player>(reader);
            reader.Close();
            
            return players;
        }

        public bool AddPlayer(string username, int matchId, int team, string roomId)
        {
            const string query = "INSERT INTO player VALUES (@match_id, @username, @team)";
            
            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@match_id", matchId);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@team", team);
            var affectedRows = command.ExecuteNonQuery();
            
            var matchServer = MatchServer.GetInstance();
            matchServer.AddPlayer(roomId, username, team);

            return affectedRows > 0;
        }
    }
}