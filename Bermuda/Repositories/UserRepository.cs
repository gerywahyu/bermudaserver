using System.Collections.Generic;
using System.Data.SqlClient;
using Bermuda.Models;
using Bermuda.Utils;
using MySql.Data.MySqlClient;

namespace Bermuda.Repositories
{
    public class UserRepository
    {
        private readonly MySqlConnection _conn;

        public UserRepository(MySqlConnection conn)
        {
            _conn = conn;
        }

        public void GetUser(int userId)
        {
        }

        public User GetUser(string username)
        {
            const string query = "SELECT * FROM user WHERE username=@username";
            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@username", username);
            var reader = command.ExecuteReader();
            var users = ModelUtils.GetObjects<User>(reader);
            reader.Close();
            return users[0];
        }

        public bool CreateUser(string username, string password)
        {
            const string query = "INSERT INTO user (username, password) VALUES (@username, @password)";

            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@password", password);
            var affectedRows = command.ExecuteNonQuery();

            return affectedRows > 0;
        }
    }
}