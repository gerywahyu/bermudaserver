using Bermuda.Models;
using Bermuda.Models.Digger;
using Bermuda.Models.Map;
using Bermuda.Utils;
using MySql.Data.MySqlClient;

namespace Bermuda.Repositories
{
    public class MapRepository
    {
        private readonly MySqlConnection _conn;
        private readonly DiggerConfig _defaultDiggerConfig = new DiggerConfig(2, 2, 2, 2, 2, 2);

        public MapRepository(MySqlConnection conn)
        {
            _conn = conn;
        }

        public Map GetMap(int matchId)
        {
            const string query = "SELECT * FROM map WHERE id=@match_id";

            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@match_id", matchId);
            var maps = ModelUtils.GetObjects<Map>(command.ExecuteReader());

            if (maps.Count == 0)
            {
                return CreateMap(matchId) ? GetMap(matchId) : null;
            }

            return maps[0];
        }

        public bool CreateMap(int matchId)
        {
            var map = RandomMapGenerator.GenerateMap(20, 20, _defaultDiggerConfig, 2);

            const string query = "INSERT INTO map (id, height, width, data) VALUES(@id, @height, @width, @data)";

            var command = new MySqlCommand(query, _conn);
            command.Parameters.AddWithValue("@id", matchId);
            command.Parameters.AddWithValue("@height", map.Height);
            command.Parameters.AddWithValue("@width", map.Width);
            command.Parameters.AddWithValue("@data", map.Data);
            var affectedRows = command.ExecuteNonQuery();

            return affectedRows > 0;
        }
    }
}