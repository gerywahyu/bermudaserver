﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Bermuda.Game;
using Bermuda.Models;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NetMQ;
using NetMQ.Sockets;
using Newtonsoft.Json;

namespace Bermuda
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Task.Run(() => CreatePubSubServer());
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .UseUrls("http://0.0.0.0:5000");

        private static void CreatePubSubServer()
        {
            using (var pubSocket = new PublisherSocket())
            {
                Console.WriteLine("Publisher Socket binding...");
                pubSocket.Options.SendHighWatermark = 1000;
                pubSocket.Bind("tcp://*:12345");

                var server = MatchServer.GetInstance();

                while (true)
                {
                    var startTime = Environment.TickCount;
                    // Put the procedure here
                    Console.WriteLine("Hello");
                    foreach (var key in server.Rooms.Keys)
                    {
                        switch (server.Rooms[key].Status)
                        {
                            case "start":
                                pubSocket.SendMoreFrame(key)
                                    .SendFrame(JsonConvert.SerializeObject(new StartEvent()));
                                break;
                            case "active":
                            {
                                var content = JsonConvert.SerializeObject(server.Rooms[key].GetGameState());
                                Console.WriteLine(content);
                                pubSocket.SendMoreFrame(key)
                                    .SendFrame(content);
                                break;
                            }
                        }
                    }
                    
                    var endTime = Environment.TickCount;
                    if (startTime - endTime > 20)
                    {
                        Thread.Sleep(20 - (startTime - endTime));
                    }
                }
            }
        }

        public static void CreateRequestServer()
        {
        }
    }
}