using MySql.Data.MySqlClient;

namespace Bermuda.Utils
{
    public static class DatabaseUtils
    {
        private static MySqlConnection _conn = null;

        public static MySqlConnection GetDbConnection()
        {
            if (_conn != null) return _conn;
            const string host = "localhost";
            const int port = 3306;
            const string database = "bermuda";
            const string username = "root";
            const string password = "";
            var connectionString = "Server=" + host + ";Database=" + database + ";port=" + port + ";User Id=" +
                                   username + ";password=" + password;
            _conn = new MySqlConnection(connectionString);
            _conn.Open();

            return _conn;
        }
    }
}