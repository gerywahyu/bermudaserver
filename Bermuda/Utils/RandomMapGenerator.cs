using Bermuda.Models.Digger;
using Bermuda.Models.Map;

namespace Bermuda.Utils
{
    public static class RandomMapGenerator
    {
        
        public static Models.Map.Map GenerateMap(int height, int width, DiggerConfig config, int multiplier)
        {
            var builder = new MapBuilder();
            builder.GenerateMap(height, width);
            
            builder.AddHallDigger(config.NumHallDigger);
            builder.AddEdgeDigger(config.NumEdgeDigger);
            builder.AddPathDigger(config.NumPathDigger);
            
            builder.RunHallDigger(config.HallPercent);
            builder.RunEdgeDigger(config.EdgePercent);
            builder.RunPathDigger(config.PathPercent);
            
            builder.ExpandMaze(multiplier);
            builder.GenerateMap();
            var map = builder.GetMap();

            return map;
        }
    }
}