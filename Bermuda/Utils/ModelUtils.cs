using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using MySql.Data.MySqlClient;
using Newtonsoft.Json;

namespace Bermuda.Utils
{
    public static class ModelUtils
    {
        private static readonly Random Random = new Random();

        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length).Select(s => s[Random.Next(s.Length)]).ToArray());
        }

        private static IEnumerable<Dictionary<string, object>> Serialize(IDataReader reader)
        {
            var results = new List<Dictionary<string, object>>();
            var cols = new List<string>();
            for (var i = 0; i < reader.FieldCount; i++)
            {
                cols.Add(reader.GetName(i));
            }

            while (reader.Read())
            {
                results.Add(SerializeRow(cols, reader));
            }

            return results;
        }

        public static List<T> GetObjects<T>(MySqlDataReader reader)
        {
            var result = Serialize(reader);
            var json = JsonConvert.SerializeObject(result, Formatting.Indented);
            return JsonConvert.DeserializeObject<List<T>>(json, new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore
            });
        }

        private static Dictionary<string, object> SerializeRow(IEnumerable<string> cols, IDataRecord reader)
        {
            return cols.ToDictionary(col => col, col => reader[col]);
        }
    }
}