using System;
using System.Collections.Generic;
using Bermuda.Models.Map;

namespace Bermuda.Models.Digger
{
    public class Digger
    {
        private const string VisitedMark = " ";
        protected static MapBlueprint Maze;
        protected static readonly Random RandomGenerator = new Random();

        private int _row;
        private int _col;
        private Action _lastMove;
        private int _changeDirectionChance;

        public Digger(MapBlueprint maze)
        {
            if (Maze == null)
            {
                Maze = maze;
            }
        }

        public void Move()
        {
            if ((_lastMove == null) || (AllowedRandomMove()))
            {
                RandomMove();
            }
            else
            {
                _lastMove();
            }

            Mark();
        }

        /* =================================================
                            Setter
        ================================================= */

        protected void SetStartPosition(int row, int col)
        {
            _row = row;
            _col = col;
        }

        protected void SetChangeDirectionChance(int chance)
        {
            _changeDirectionChance = chance;
        }

        /* =================================================
                            Member Method
        ================================================= */

        private void Mark()
        {
            var playArea = Maze.GetPlayArea();
            playArea[_row][_col] = VisitedMark;
        }

        private bool HasVisited(int row, int col)
        {
            var playArea = Maze.GetPlayArea();
            return playArea[row][col] == VisitedMark;
        }

        private void MoveLeft()
        {
            if (!AbleToMoveLeft()) return;
            _col--;
            _lastMove = MoveLeft;
        }

        private void MoveRight()
        {
            if (!AbleToMoveRight()) return;
            _col++;
            _lastMove = MoveRight;
        }

        private void MoveUp()
        {
            if (!AbleToMoveUp()) return;
            _row--;
            _lastMove = MoveUp;
        }

        private void MoveDown()
        {
            if (!AbleToMoveDown()) return;
            _row++;
            _lastMove = MoveDown;
        }

        private bool AllowedRandomMove()
        {
            return (RandomGenerator.Next(0, 100) < _changeDirectionChance);
        }

        private void RandomMove()
        {
            var movement = GetPreferredDirection();

            var randInt = RandomGenerator.Next(movement.Count);
            var randomMovement = movement[randInt];
            randomMovement();
        }

        private List<Action> GetPreferredDirection()
        {
            var moveDirection = new List<Action>();
            var possibleDirection = new List<Action>();

            if (AbleToMoveLeft())
            {
                if (HasNotVisitedLeft())
                {
                    moveDirection.Add(MoveLeft);
                }

                possibleDirection.Add(MoveLeft);
            }

            if (AbleToMoveRight())
            {
                if (HasNotVisitedRight())
                {
                    moveDirection.Add(MoveRight);
                }

                possibleDirection.Add(MoveRight);
            }

            if (AbleToMoveUp())
            {
                if (HasNotVisitedUp())
                {
                    moveDirection.Add(MoveUp);
                }

                possibleDirection.Add(MoveUp);
            }

            if (AbleToMoveDown())
            {
                if (HasNotVisitedDown())
                {
                    moveDirection.Add(MoveDown);
                }

                possibleDirection.Add(MoveDown);
            }

            if (moveDirection.Count == 0)
            {
                moveDirection = possibleDirection;
            }

            return moveDirection;
        }

        private bool AbleToMoveLeft()
        {
            return _col > 0;
        }

        private bool AbleToMoveRight()
        {
            return _col < Maze.GetWidth() - 1;
        }

        private bool AbleToMoveUp()
        {
            return _row > 0;
        }

        private bool AbleToMoveDown()
        {
            return _row < Maze.GetHeight() - 1;
        }

        private bool HasNotVisitedLeft()
        {
            return !(HasVisited(_row, _col - 1));
        }

        private bool HasNotVisitedRight()
        {
            return !(HasVisited(_row, _col + 1));
        }

        private bool HasNotVisitedUp()
        {
            return !(HasVisited(_row - 1, _col));
        }

        private bool HasNotVisitedDown()
        {
            return !(HasVisited(_row + 1, _col));
        }
    }

/* =================================================
                    Child Classes
================================================= */

    public class PathDigger : Digger
    {
        private readonly int _changeDirectionChance = RandomGenerator.Next(40, 80);

        public PathDigger(MapBlueprint maze) : base(maze)
        {
            SetInitPosition();
            SetChangeDirectionChance(_changeDirectionChance);
        }

        public void SetInitPosition()
        {
            var mazeHeight = Maze.GetHeight();
            var mazeWidth = Maze.GetWidth();

            // Randomly placed in the middle 80 % of the map
            var row = RandomGenerator.Next((int) (mazeHeight * 0.1), (int) (mazeHeight * 0.9));
            var col = RandomGenerator.Next((int) (mazeWidth * 0.1), (int) (mazeWidth * 0.9));
            SetStartPosition(row, col);
        }
    }

    public class HallDigger : Digger
    {
        private readonly int _changeDirectionChance = RandomGenerator.Next(80, 130);

        public HallDigger(MapBlueprint maze) : base(maze)
        {
            SetInitPosition();
            SetChangeDirectionChance(_changeDirectionChance);
        }

        public void SetInitPosition()
        {
            var mazeHeight = Maze.GetHeight();
            var mazeWidth = Maze.GetWidth();

            // Randomly placed in the middle 40 % of the map
            var row = RandomGenerator.Next((int) (mazeHeight * 0.3), (int) (mazeHeight * 0.7));
            var col = RandomGenerator.Next((int) (mazeWidth * 0.3), (int) (mazeWidth * 0.7));
            SetStartPosition(row, col);
        }
    }

    public class EdgeDigger : Digger
    {
        private readonly int _changeDirectionChance = RandomGenerator.Next(80, 130);

        public EdgeDigger(MapBlueprint maze) : base(maze)
        {
            SetInitPosition();
            SetChangeDirectionChance(_changeDirectionChance);
        }

        private void SetInitPosition()
        {
            var mazeHeight = Maze.GetHeight();
            var mazeWidth = Maze.GetWidth();

            var mazeEdge = RandomGenerator.Next(0, 3);
            var row = 0;
            var col = 0;

            switch (mazeEdge)
            {
                case 0:
                    row = RandomGenerator.Next(0, (int) (mazeHeight * 0.05));
                    col = RandomGenerator.Next(0, mazeWidth - 1);
                    break;
                case 1:
                    row = RandomGenerator.Next((int) (mazeHeight * 0.95), mazeHeight - 1);
                    col = RandomGenerator.Next(0, mazeWidth - 1);
                    break;
                case 2:
                    row = RandomGenerator.Next(0, mazeHeight - 1);
                    col = RandomGenerator.Next(0, (int) (mazeWidth * 0.05));
                    break;
                case 3:
                    row = RandomGenerator.Next(0, mazeHeight - 1);
                    col = RandomGenerator.Next((int) (mazeWidth * 0.95), mazeWidth - 1);
                    break;
            }

            SetStartPosition(row, col);
        }
    }
}