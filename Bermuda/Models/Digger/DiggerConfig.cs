namespace Bermuda.Models.Digger
{
    public class DiggerConfig
    {
        public int NumPathDigger;
        public int NumHallDigger;
        public int NumEdgeDigger;

        public float PathPercent;
        public float HallPercent;
        public float EdgePercent;

        public DiggerConfig(int numPathDigger, int numHallDigger, int numEdgeDigger, float pathPercent, float hallPercent, float edgePercent)
        {
            NumPathDigger = numPathDigger;
            NumHallDigger = numHallDigger;
            NumEdgeDigger = numEdgeDigger;
            PathPercent = pathPercent;
            HallPercent = hallPercent;
            EdgePercent = edgePercent;
        }
    }
}