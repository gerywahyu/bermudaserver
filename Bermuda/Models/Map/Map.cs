using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bermuda.Models.Map
{

    public class Map
    {
        [JsonProperty(PropertyName = "height")]
        public int Height { get; set; }
        
        [JsonProperty(PropertyName = "width")]
        public int Width { get; set; }
        
        [JsonProperty(PropertyName = "data")]
        public string Data { get; set; }
        private List<List<string>> _dataArray;

        public Map(int height, int width)
        {
            this.Height = height;
            this.Width = width;
        }

        public List<List<string>> GetAsArray()
        {
            return _dataArray ?? (_dataArray = JsonConvert.DeserializeObject<List<List<string>>>(Data));
        }
    }
}