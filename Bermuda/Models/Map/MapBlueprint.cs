using System;
using System.Collections.Generic;

namespace Bermuda.Models.Map
{
    public class MapBlueprint
    {
        private const string WallSymbol = "@";

        private int _height;
        private int _width;
        public List<List<string>> PlayArea = new List<List<string>>();

        public MapBlueprint(int height, int width)
        {
            _height = height;
            _width = width;
            CreatePlayArea();
        }

        /* =================================================
                            Getter Setter
        ================================================= */

        public string GetTile(int row, int col)
        {
            try
            {
                return PlayArea[row][col];
            }
            catch (Exception)
            {
                return null;
            }
        }

        public string GetTileType(int row, int col)
        {
            if (IsTileWall(row, col))
            {
                return "Wall";
            }

            /*
                Grid for checking wall relative to current tile (x)
                    | 1 | 2 | 3 |
                    | 4 | x | 6 |
                    | 7 | 8 | 9 |
            */

            // Unity top and bottom is reversed ,, normal cartesian
            bool isWall01 = IsTileWall(row + 1, col - 1);
            bool isWall02 = IsTileWall(row + 1, col + 0);
            bool isWall03 = IsTileWall(row + 1, col + 1);
            bool isWall04 = IsTileWall(row + 0, col - 1);
            bool isWall06 = IsTileWall(row + 0, col + 1);
            bool isWall07 = IsTileWall(row - 1, col - 1);
            bool isWall08 = IsTileWall(row - 1, col + 0);
            bool isWall09 = IsTileWall(row - 1, col + 1);

            /* =====   Check for corner tile   ===== */
            if (isWall08 && isWall06)
            {
                return "CornerTopLeft";
            }

            if (isWall08 && isWall04)
            {
                return "CornerTopRight";
            }

            if (isWall02 && isWall06)
            {
                return "CornerBottomLeft";
            }

            if (isWall02 && isWall04)
            {
                return "CornerBottomRight";
            }

            /* =====   Check for edge tile   ===== */
            if (isWall09 && !isWall08 && !isWall06)
            {
                return "EdgeTopLeft";
            }

            if (isWall07 && !isWall08 && !isWall04)
            {
                return "EdgeTopRight";
            }

            if (isWall03 && !isWall02 && !isWall06)
            {
                return "EdgeBottomLeft";
            }

            if (isWall01 && !isWall02 && !isWall04)
            {
                return "EdgeBottomRight";
            }

            /* =====    Check for side tile   ===== */
            if (isWall08)
            {
                return "SideTop";
            }

            if (isWall02)
            {
                return "SideBottom";
            }

            if (isWall06)
            {
                return "SideLeft";
            }

            if (isWall04)
            {
                return "SideRight";
            }

            return "Empty";
        }

        public int GetHeight()
        {
            return _height;
        }

        public int GetWidth()
        {
            return _width;
        }

        public List<List<string>> GetPlayArea()
        {
            return PlayArea;
        }

        public void SetHeight(int height)
        {
            _height = height;
        }

        public void SetWidth(int width)
        {
            _width = width;
        }

        public void SetPlayArea(List<List<string>> playArea)
        {
            this.PlayArea = playArea;
        }

        /* =================================================
                            Member Method
        ================================================= */

        private bool IsTileEmpty(int row, int col)
        {
            return !(IsTileWall(row, col));
        }

        private bool IsTileWall(int row, int col)
        {
            return GetTile(row, col) == WallSymbol;
        }

        private void CreatePlayArea()
        {
            for (var i = 0; i < _height; i++)
            {
                var row = new List<string>();
                for (var j = 0; j < _width; j++)
                {
                    row.Add(WallSymbol);
                }

                PlayArea.Add(row);
            }
        }
    }
}