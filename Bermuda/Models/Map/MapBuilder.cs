using System.Collections.Generic;
using Bermuda.Models.Digger;
using Newtonsoft.Json;

namespace Bermuda.Models.Map
{
    public class MapBuilder
    {
        private MapBlueprint _mapBlueprint;
        private Map _map;

        private int _mapHeight;
        private int _mapWidth;

        private readonly List<PathDigger> _pathDiggers = new List<PathDigger>();
        private readonly List<HallDigger> _hallDiggers = new List<HallDigger>();
        private readonly List<EdgeDigger> _edgeDiggers = new List<EdgeDigger>();

        public void GenerateMap(int height, int width)
        {
            _mapBlueprint = new MapBlueprint(height, width);
            _mapHeight = height;
            _mapWidth = width;
        }

        public void AddPathDigger(int numberOfDigger)
        {
            for (var i = 0; i < numberOfDigger; i++)
            {
                var digger = new PathDigger(_mapBlueprint);
                _pathDiggers.Add(digger);
            }
        }

        public void AddHallDigger(int numberOfDigger)
        {
            for (var i = 0; i < numberOfDigger; i++)
            {
                var digger = new HallDigger(_mapBlueprint);
                _hallDiggers.Add(digger);
            }
        }

        public void AddEdgeDigger(int numberOfDigger)
        {
            for (var i = 0; i < numberOfDigger; i++)
            {
                var digger = new EdgeDigger(_mapBlueprint);
                _edgeDiggers.Add(digger);
            }
        }

        public void RunPathDigger(float percentage)
        {
            var stepRequired = (int) (_mapWidth * _mapHeight * percentage);
            var step = 0;

            while (step < stepRequired)
            {
                foreach (var digger in _pathDiggers)
                {
                    digger.Move();
                    step++;
                }
            }
        }

        public void RunHallDigger(float percentage)
        {
            var stepRequired = (int) (_mapWidth * _mapHeight * percentage);
            var step = 0;

            while (step < stepRequired)
            {
                foreach (var digger in _hallDiggers)
                {
                    digger.Move();
                    step++;
                }
            }
        }

        public void RunEdgeDigger(float percentage)
        {
            var stepRequired = (int) (_mapWidth * _mapHeight * percentage);
            var step = 0;

            while (step < stepRequired)
            {
                foreach (var digger in _edgeDiggers)
                {
                    digger.Move();
                    step++;
                }
            }
        }

        public void ExpandMaze(int expandMultiplier = 2)
        {
            var expandedPlayArea = new List<List<string>>();

            // for each original row
            for (var i = 0; i < _mapHeight; i++)
            {
                var newRow = new List<string>();

                // for each original column
                for (var j = 0; j < _mapWidth; j++)
                {
                    var tile = _mapBlueprint.GetTile(i, j);

                    // Add tile col(repeat n many times)
                    for (var k = 0; k < expandMultiplier; k++)
                    {
                        newRow.Add(tile);
                    }
                }

                // Add tile row(repeat n many times)
                for (var l = 0; l < expandMultiplier; l++)
                {
                    expandedPlayArea.Add(newRow);
                }
            }

            _mapBlueprint.SetPlayArea(expandedPlayArea);
            _mapHeight *= expandMultiplier;
            _mapWidth *= expandMultiplier;
            _mapBlueprint.SetHeight(_mapHeight);
            _mapBlueprint.SetWidth(_mapWidth);
        }

        public void GenerateMap()
        {
            _map = new Map(_mapBlueprint.GetHeight(), _mapBlueprint.GetWidth())
            {
                Data = JsonConvert.SerializeObject(_mapBlueprint.PlayArea)
            };
        }

        public MapBlueprint GetBlueprint()
        {
            return _mapBlueprint;
        }

        public Map GetMap()
        {
            return _map;
        }
    }
}