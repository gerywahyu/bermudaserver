using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Bermuda.Models.Responses
{
    public class UpdateInstanceResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        public UpdateInstanceResponse(bool success)
        {
            Success = success;
        }
    }
}