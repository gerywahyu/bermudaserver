using Newtonsoft.Json;

namespace Bermuda.Models.Responses
{
    public class CreateUserResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        public CreateUserResponse(bool success)
        {
            Success = success;
        }
    }
}