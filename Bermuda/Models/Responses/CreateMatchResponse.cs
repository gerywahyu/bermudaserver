using Newtonsoft.Json;

namespace Bermuda.Models.Responses
{
    public class CreateMatchResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }
        
        [JsonProperty(PropertyName = "match_id")]
        public int MatchId {get; set;}

        [JsonProperty(PropertyName = "room_id")]
        public string RoomId { get; set; }

        public CreateMatchResponse(bool success, Match match)
        {
            Success = success;
            MatchId = match.Id;
            RoomId = match.RoomId;
        }
    }
}