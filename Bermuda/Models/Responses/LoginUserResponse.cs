using Newtonsoft.Json;

namespace Bermuda.Models.Responses
{
    public class LoginUserResponse
    {
        [JsonProperty(PropertyName = "success")]
        public bool Success { get; set; }

        public LoginUserResponse(bool success)
        {
            Success = success;
        }
    }
}