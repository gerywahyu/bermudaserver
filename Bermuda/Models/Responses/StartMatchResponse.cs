using Newtonsoft.Json;

namespace Bermuda.Models.Responses
{
    public class StartMatchResponse
    {
        [JsonProperty(PropertyName = "start")]
        public bool Start { get; set; }

        public StartMatchResponse(bool start)
        {
            Start = start;
        }
    }
}