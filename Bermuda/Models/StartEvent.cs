using Newtonsoft.Json;

namespace Bermuda.Models
{
    public class StartEvent
    {
        [JsonProperty(PropertyName = "start")] public bool Start = true;
    }
}