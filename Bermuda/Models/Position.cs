using Newtonsoft.Json;

namespace Bermuda.Models
{
    public class Position
    {
        [JsonProperty(PropertyName = "x")] public int X { get; set; }

        [JsonProperty(PropertyName = "y")] public int Y { get; set; }
    }
}