using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Bermuda.Models
{
    public class Player
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        
        [JsonProperty(PropertyName = "team")]
        public int Team { get; set; }

        public Player(string username, int team)
        {
            Username = username;
            Team = team;
        }
    }
}