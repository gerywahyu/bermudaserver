using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Bermuda.Models
{
    public class Match
    {
        [JsonProperty(PropertyName = "id")] public int Id { get; set; }

        [JsonProperty(PropertyName = "master_username")]
        public string MasterUsername { get; set; }

        [JsonProperty(PropertyName = "created_at")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty(PropertyName = "winner")]
        public int Winner { get; set; }

        [JsonProperty(PropertyName = "room_id")]
        public string RoomId { get; set; }
        
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        [JsonProperty(PropertyName = "players")]
        public List<Player> Players { get; set; }
    }
}