using Newtonsoft.Json;

namespace Bermuda.Models.Requests
{
    public class StartMatchRequest
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }
        
        [JsonProperty(PropertyName = "room_id")]
        public string RoomId { get; set; }
    }
}