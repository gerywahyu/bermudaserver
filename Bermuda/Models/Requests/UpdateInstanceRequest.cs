using System.Collections.Generic;
using Bermuda.Game;
using Newtonsoft.Json;

namespace Bermuda.Models.Requests
{
    public class UpdateInstanceRequest
    {
        [JsonProperty(PropertyName = "roomId")]
        public string RoomId { get; set; }
        
        [JsonProperty(PropertyName = "instances")]
        public List<ObjectInstance> Instances { get; set; }
    }
}