using Newtonsoft.Json;

namespace Bermuda.Models.Requests
{
    public class LoginUserRequest
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }
    }
}