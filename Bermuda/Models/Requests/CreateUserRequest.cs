using Newtonsoft.Json;

namespace Bermuda.Models.Requests
{
    public class CreateUserRequest
    {
        [JsonProperty(PropertyName = "username")]
        public string Username { get; set; }

        [JsonProperty(PropertyName = "password")]
        public string Password { get; set; }        
    }
}