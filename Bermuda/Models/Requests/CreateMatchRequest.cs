using Newtonsoft.Json;

namespace Bermuda.Models.Requests
{
    public class CreateMatchRequest
    {
        [JsonProperty(PropertyName = "username")]
        public string Username;

        public CreateMatchRequest(string username)
        {
            Username = username;
        }
    }
}